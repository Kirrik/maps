package com.k.maps;

import android.location.LocationManager;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

public class MainActivity extends AppCompatActivity {

    GoogleMap map;
    private LocationManager locationManager;
    Marker marker;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final LatLng KHARKOV = new LatLng(49.9944422, 36.2368201);
        map = ((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map_view)).getMap();
        marker = map.addMarker(new MarkerOptions().position(KHARKOV).title("Kharkov")); //create marker
        map.moveCamera(CameraUpdateFactory.newLatLngZoom(KHARKOV, 15)); // Move the camera instantly to Kharkov with a zoom of 15.
        map.animateCamera(CameraUpdateFactory.zoomTo(15), 2000, null); // Zoom in
    }
}
